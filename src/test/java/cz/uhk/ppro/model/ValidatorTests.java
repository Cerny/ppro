package cz.uhk.ppro.model;

import cz.uhk.ppro.entities.User;
import org.junit.jupiter.api.Test;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.Locale;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

class ValidatorTests {

    private Validator createValidator() {
        LocalValidatorFactoryBean localValidatorFactoryBean = new LocalValidatorFactoryBean();
        localValidatorFactoryBean.afterPropertiesSet();
        return localValidatorFactoryBean;
    }

    @Test
    void shouldNotValidateWhenFirstNameEmpty() {

        LocaleContextHolder.setLocale(Locale.ENGLISH);
        User person = new User("x@y.cz", "", "Karel", "Novotný");

        Validator validator = createValidator();
        Set<ConstraintViolation<User>> constraintViolations = validator.validate(person);

        assertThat(constraintViolations.size()).isEqualTo(1);
    }

}
