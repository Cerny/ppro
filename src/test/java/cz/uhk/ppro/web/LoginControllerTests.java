package cz.uhk.ppro.web;

import cz.uhk.ppro.controllers.LoginController;
import cz.uhk.ppro.entities.User;
import cz.uhk.ppro.form.LoginForm;
import cz.uhk.ppro.service.VocabularyServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.support.FormattingConversionServiceFactoryBean;
import org.springframework.test.context.junit.jupiter.web.SpringJUnitWebConfig;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import java.util.Arrays;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import static org.mockito.BDDMockito.given;

@SpringJUnitWebConfig(locations = {"classpath:spring/mvc-test-config.xml", "classpath:spring/mvc-core-config.xml"})
public class LoginControllerTests {

    @Autowired
    private LoginController loginController;

    @Autowired
    private FormattingConversionServiceFactoryBean formattingConversionServiceFactoryBean;

    @Autowired
    private VocabularyServiceImpl vocabularyService;

    private MockMvc mockMvc;

    @BeforeEach
    void setup() {
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setPrefix("/WEB-INF/views/");
        viewResolver.setSuffix(".jsp");

        this.mockMvc = MockMvcBuilders.
                standaloneSetup(loginController)
                .setViewResolvers(viewResolver)
                .setConversionService(formattingConversionServiceFactoryBean.getObject())
                .build();

        User user = new User("x@y.cz", "$2a$10$5J1Defv2DydGwf2sPn/QOOQb35Ix8vFJCJTnyd3Ve8YD2nOAxR6Eu", "Jiří", "Novotný");
        given(this.vocabularyService.findUserByEmail("x@y.cz")).willReturn(user);
    }

    @Test
    void testLoginWithCorrectCredentials() throws Exception {
        LoginForm loginForm = new LoginForm();
        loginForm.setEmail("x@y.cz");
        loginForm.setPassword("123");

        mockMvc.perform(post("/login")
                .flashAttr("loginForm", loginForm)
        )
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/"));
    }

    @Test
    void testLoginWithWrongCredentials() throws Exception {
        LoginForm loginForm = new LoginForm();
        loginForm.setEmail("x@y.cz");
        loginForm.setPassword("1234");

        mockMvc.perform(post("/login")
                .flashAttr("loginForm", loginForm)
        )
                .andExpect(status().isOk())
                .andExpect(view().name("login"));
    }

    @Test
    void testLogin() throws Exception {
        mockMvc.perform(get("/login"))
                .andExpect(status().isOk())
                .andExpect(view().name("login"));
    }

}
