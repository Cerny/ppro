package cz.uhk.ppro.web;

import cz.uhk.ppro.controllers.HomeController;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit.jupiter.web.SpringJUnitWebConfig;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringJUnitWebConfig(locations = {"classpath:spring/mvc-test-config.xml", "classpath:spring/mvc-core-config.xml"})
public class HomeControllerTests {

    @Autowired
    private HomeController homeController;

    private MockMvc mockMvc;

    @BeforeEach
    void setup() {
        this.mockMvc = MockMvcBuilders.standaloneSetup(homeController).build();
    }

    @Test
    void testNotLoggedUserHome() throws Exception {
        mockMvc.perform(get("/"))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/login"));
    }

    @Test
    void testLoggedUserHome() throws Exception {
        mockMvc.perform(get("/")
                .sessionAttr("Email", "test@gmail.com")
        )
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/lists"));
    }

}
