package cz.uhk.ppro.web;

import cz.uhk.ppro.controllers.LoginController;
import cz.uhk.ppro.controllers.RegisterController;
import cz.uhk.ppro.entities.User;
import cz.uhk.ppro.form.LoginForm;
import cz.uhk.ppro.form.RegisterForm;
import cz.uhk.ppro.service.VocabularyServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.support.FormattingConversionServiceFactoryBean;
import org.springframework.test.context.junit.jupiter.web.SpringJUnitWebConfig;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import java.util.Arrays;
import java.util.List;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringJUnitWebConfig(locations = {"classpath:spring/mvc-test-config.xml", "classpath:spring/mvc-core-config.xml"})
public class RegisterControllerTests {

    @Autowired
    private RegisterController registerController;

    @Autowired
    private FormattingConversionServiceFactoryBean formattingConversionServiceFactoryBean;

    @Autowired
    private VocabularyServiceImpl vocabularyService;

    private MockMvc mockMvc;

    @BeforeEach
    void setup() {
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setPrefix("/WEB-INF/views/");
        viewResolver.setSuffix(".jsp");

        this.mockMvc = MockMvcBuilders.
                standaloneSetup(registerController)
                .setViewResolvers(viewResolver)
                .setConversionService(formattingConversionServiceFactoryBean.getObject())
                .build();
    }

    @Test
    void testSuccessfulRegister() throws Exception {
        RegisterForm registerForm = new RegisterForm();
        registerForm.setName("Jiří");
        registerForm.setSurname("Novák");
        registerForm.setEmail("x@y.cz");
        registerForm.setPassword("123");
        registerForm.setConfirmPassword("123");

        mockMvc.perform(post("/register")
                .flashAttr("registerForm", registerForm)
        )
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/"));
    }

    @Test
    void testRegisterUserAlreadyExists() throws Exception {
        String usersEmail = "x@y.cz";
        User user = new User(usersEmail, "123", "Jiří", "Novotný");
        given(this.vocabularyService.findUserByEmail(usersEmail)).willReturn(user);

        RegisterForm registerForm = new RegisterForm();
        registerForm.setName("Jiří");
        registerForm.setSurname("Novák");
        registerForm.setEmail(usersEmail);
        registerForm.setPassword("123");
        registerForm.setConfirmPassword("123");

        mockMvc.perform(post("/register")
                .flashAttr("registerForm", registerForm)
        )
                .andExpect(status().isOk())
                .andExpect(model().attributeHasErrors("registerForm"))
                .andExpect(view().name("register"));
    }

    @Test
    void testRegisterScreen() throws Exception {
        LoginForm loginForm = new LoginForm();
        loginForm.setEmail("x@y.cz");
        loginForm.setPassword("123");

        mockMvc.perform(get("/register"))
                .andExpect(status().isOk())
                .andExpect(view().name("register"));
    }

}

