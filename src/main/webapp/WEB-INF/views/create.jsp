<%--
  Created by IntelliJ IDEA.
  User: Honza
  Date: 07.01.2020
  Time: 17:04
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page session="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="vocabulary" tagdir="/WEB-INF/tags" %>

<vocabulary:layout pageName="createWordList">

    <div class="container">
        <h2>Create new word list</h2>
        <form:form id="create-form" modelAttribute="wordListForm" action="/create" method="post" role="form">
            <div class="form-group">
                <label>Name</label>
                <input autocomplete="off" type="text" name="name" id="name" class="form-control">
                <label>Description</label>
                <input autocomplete="off" type="text" name="description" id="description" class="form-control">
                <br>
                <label>Language 1</label>
                <select name="iso1" id="iso1">
                    <c:forEach items="${languages}" var="lang">
                        <option value="${lang.getIso()}">${lang.getName()}</option>
                    </c:forEach>
                </select>
                <label>Language 2</label>
                <select name="iso2" id="iso2">
                    <c:forEach items="${languages}" var="lang">
                        <option value="${lang.getIso()}">${lang.getName()}</option>
                    </c:forEach>
                </select>
                <br>
                <br>
                <c:set var="formHasRegisterError">
                    <form:errors/>
                </c:set>
                <c:if test="${not empty formHasRegisterError}">
                    <div class="alert alert-danger">
                        <i class="fa fa-ban"></i>&nbsp;${formHasRegisterError}
                    </div>
                </c:if>
                <button type="submit" class="btn btn-default">Create</button>
            </div>

        </form:form>


    </div>

</vocabulary:layout>
