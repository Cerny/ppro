<%--
  Created by IntelliJ IDEA.
  User: michalcerny
  Date: 23/12/2019
  Time: 18:39
  To change this template use File | Settings | File Templates.
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page session="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="vocabulary" tagdir="/WEB-INF/tags" %>

<vocabulary:layout pageName="statistics">
    <h2>Statistics</h2>

    <table id="statisticsTable" class="table table-striped">
        <thead>
        <tr>
            <th style="width: 150px;">Date</th>
            <th style="width: 150px;">Wrong count</th>
            <th style="width: 150px;">Correct count</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${practiceDays}" var="day">
            <tr>
                <td><fmt:formatDate pattern="dd.MM.yyyy" value="${day.practiceDayPK.date}"/></td>
                <td>${day.wrongCount}</td>
                <td>${day.correctCount}</td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</vocabulary:layout>