<%--
  Created by IntelliJ IDEA.
  User: michalcerny
  Date: 23/12/2019
  Time: 14:46
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page session="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="vocabulary" tagdir="/WEB-INF/tags" %>

<vocabulary:layout pageName="practice">
    <div class="container">
        <form:form modelAttribute="wordForm" action="/practice/next" method="post">
            <div class="form-group">
                <label for="exampleInputPassword1">${wordForTranslation.spelling}</label>
                <input type="word" name="word" id="word" class="form-control" id="exampleInputPassword1"
                       disabled="true" placeholder=${previousAnswer}>
            </div>

            <c:choose>
                <c:when test="${not empty correctAnswer and correctAnswer}">
                    <div class="alert alert-success" role="alert">
                        Correct answer
                    </div>
                </c:when>
                <c:when test="${not empty correctAnswer and !correctAnswer}">
                    <div class="alert alert-danger" role="alert">
                        Wrong answer. Correct answer is "${wordTranslated.spelling}"
                    </div>
                </c:when>
            </c:choose>

            <button type="submit" class="btn btn-default">Continue</button>
        </form:form>
        <p style="margin-bottom:10px;"></p>
        <a href="/practice/finish" class="btn btn-default">Finish</a>
    </div>
</vocabulary:layout>