<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page session="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="vocabulary" tagdir="/WEB-INF/tags" %>

<vocabulary:layout pageName="practice">
    <div class="container">
        <form:form modelAttribute="wordForm" action="/practice/check" method="post">
            <div class="form-group">
                <label for="exampleInputPassword1">${wordForTranslation.spelling}</label>
                <input autocomplete="off" type="word" name="word" id="word" class="form-control" id="exampleInputPassword1"
                       placeholder="Type here">
            </div>
            <button type="submit" class="btn btn-default">Check answer</button>
        </form:form>
        <p style="margin-bottom:10px;"></p>
        <a href="/practice/skip" class="btn btn-default">Skip</a>
    </div>
</vocabulary:layout>