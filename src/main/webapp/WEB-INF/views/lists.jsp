<%--
  Created by IntelliJ IDEA.
  User: Honza
  Date: 11.12.2019
  Time: 19:44
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page session="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="vocabulary" tagdir="/WEB-INF/tags" %>

<vocabulary:layout pageName="wordLists">
    <%--    List JSP - Logged in user: <%= session.getAttribute("Email") %> <br>--%>

    <h2>Your word lists</h2>
    <table id="usersLists" class="table table-striped">
        <thead>
        <tr>
            <th>Word Lists</th>
            <th></th>
            <th></th>
            <th align="right"><a href="/allLists">Add</a></th>
        </tr>
        <tr>
            <th style="width: 200px;">Name</th>
            <th style="width: 100px;">Language 1</th>
            <th style="width: 100px;">Language 2</th>
            <th style="width: 250px;">Description</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${users}" var="users">
            <tr>
                <td>
                    <spring:url value="/practiceOverview/{wordListId}.html" var="wordListUrl">
                        <spring:param name="wordListId" value="${users.id}"/>
                    </spring:url>
                    <a href="${fn:escapeXml(wordListUrl)}">${users.getName()}</a></td>
                <td>${users.getLang1()}</td>
                <td>${users.getLang2()}</td>
                <td>${users.getDescription()}</td>
            </tr>
        </c:forEach>
        </tbody>
    </table>


</vocabulary:layout>
