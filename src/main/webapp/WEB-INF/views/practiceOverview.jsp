<%--
  Created by IntelliJ IDEA.
  User: michalcerny
  Date: 22/12/2019
  Time: 20:05
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page session="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="vocabulary" tagdir="/WEB-INF/tags" %>

<vocabulary:layout pageName="practiceOverview">

    <h2>Word list overview</h2>
    <table class="table table-striped">
        <tr>
            <th>Name</th>
            <td><b><c:out value="${wordList.name}"/></b></td>
        </tr>
        <tr>
            <th>Description</th>
            <td><c:out value="${wordList.description}"/></td>
        </tr>
        <tr>
            <th>Languages</th>
            <td><c:out value="${wordList.lang1}, ${wordList.lang2}"/></td>
        </tr>
    </table>

    <a href="/addWords/${wordList.id}" class="btn btn-default">Edit word list</a>
    <br><br>
    <c:choose>
        <c:when test="${not empty emptyList and emptyList}">
            <div class="alert alert-danger" role="alert">
                The word list is empty. Practice can't begin.
            </div>
        </c:when>
        <c:otherwise>
            <h2>Start practice</h2>
            <spring:url value="/practice/{wordListId}?languageFrom={languageFrom}&languageTo={languageTo}" var="practice1Url">
                <spring:param name="wordListId" value="${wordList.id}"/>
                <spring:param name="languageFrom" value="${wordList.lang1}"/>
                <spring:param name="languageTo" value="${wordList.lang2}"/>
            </spring:url>
            <a href="${fn:escapeXml(practice1Url)}" class="btn btn-default">${wordList.lang1} ⮕ ${wordList.lang2}</a>

            <spring:url value="/practice/{wordListId}?languageFrom={languageFrom}&languageTo={languageTo}" var="practice2Url">
                <spring:param name="wordListId" value="${wordList.id}"/>
                <spring:param name="languageFrom" value="${wordList.lang2}"/>
                <spring:param name="languageTo" value="${wordList.lang1}"/>
            </spring:url>
            <a href="${fn:escapeXml(practice2Url)}" class="btn btn-default">${wordList.lang2} ⮕ ${wordList.lang1}</a>
        </c:otherwise>
    </c:choose>

</vocabulary:layout>
