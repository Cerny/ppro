<%--
  Created by IntelliJ IDEA.
  User: michalcerny
  Date: 23/12/2019
  Time: 17:34
  To change this template use File | Settings | File Templates.
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page session="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="vocabulary" tagdir="/WEB-INF/tags" %>

<vocabulary:layout pageName="statisticsAfterPractice">

    <h2>Statistics</h2>
    <table id="usersLists" class="table table-striped">
        <thead>
        <tr>
            <th style="width: 200px;">Language 1</th>
            <th style="width: 200px;">Language 2</th>
            <th>Score</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${words}" var="word">
            <tr>
                <td>${word.wordForTranslation.spelling}</td>
                <td>${word.wordTranslated.spelling}</td>
                <td>${word.score}</td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
    <a href="/" class="btn btn-default">Finish</a>

</vocabulary:layout>