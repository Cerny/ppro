<%--
  Created by IntelliJ IDEA.
  User: Honza
  Date: 07.01.2020
  Time: 17:05
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page session="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="vocabulary" tagdir="/WEB-INF/tags" %>


<vocabulary:layout pageName="fillWordList">

    <div class="container">
        <h2>Fill the word list: ${list.name}</h2>

        <table class="table table-striped">
            <tbody>
                <tr>
                    <th>${list.lang1}</th>
                    <th>${list.lang2}</th>
                </tr>
                <form:form id="fill-form" modelAttribute="addWordForm" action="/addWords/${list.id}" method="post" role="form">
                <tr>
                    <td>
                        <input autocomplete="off" type="text" name="word1" id="word1" class="form-control">
                    </td>
                    <td>
                        <input autocomplete="off" type="text" name="word2" id="word2" class="form-control">
                    </td>
                </tr>

                <c:set var="formHasRegisterError">
                    <form:errors/>
                </c:set>
                <c:if test="${not empty formHasRegisterError}">
                    <tr>
                        <td colspan="2">
                            <div class="alert alert-danger">
                                <i class="fa fa-ban"></i>&nbsp;${formHasRegisterError}
                            </div>
                        </td>
                    </tr>
                </c:if>

                <tr>
                    <td>
                        <button type="submit" class="btn btn-default">Add word</button>
                    </td>
                    </form:form>
                    <td>
                        <a href="/allLists" class="btn btn-default">Finish</a>
                    </td>
                </tr>
                <tr>
                    <th>Words already in the list:</th>
                    <td></td>
                </tr>
                <c:forEach items="${words}" var="words">
                    <tr>
                        <td>${words.wordForTranslation.spelling}</td>
                        <td>${words.wordTranslated.spelling}</td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>



    </div>

</vocabulary:layout>
