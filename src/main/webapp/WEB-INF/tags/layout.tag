<%@ tag trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="vocabulary" tagdir="/WEB-INF/tags" %>

<%@ attribute name="pageName" required="true" %>
<%@ attribute name="customScript" required="false" fragment="true"%>

<!doctype html>
<html>
<vocabulary:htmlHeader/>

<body>
<vocabulary:bodyHeader menuName="${pageName}"/>

<div class="container-fluid">
    <div class="container xd-container">

        <jsp:doBody/>
    </div>
</div>
<vocabulary:footer/>
<jsp:invoke fragment="customScript" />

</body>

</html>
