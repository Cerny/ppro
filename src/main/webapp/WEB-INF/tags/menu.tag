<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="vocabulary" tagdir="/WEB-INF/tags" %>
<%@ attribute name="name" required="true" rtexprvalue="true"
              description="xxx" %>

<nav class="navbar navbar-default" role="navigation">
    <div class="container">
        <div class="navbar-collapse collapse" id="main-navbar">
            <ul class="nav navbar-nav navbar-right">

                <vocabulary:menuItem active="${name eq 'wordLists'}" url="/" title="home page">
                    <span>Home</span>
                </vocabulary:menuItem>

                <vocabulary:menuItem active="${name eq 'statistics'}" url="/statistics" title="Statistics">
                    <span>Statistics</span>
                </vocabulary:menuItem>

                <vocabulary:menuItem active="${name eq 'logout'}" url="/logout" title="Logout">
                    <span>Logout</span>
                </vocabulary:menuItem>

            </ul>
        </div>
    </div>
</nav>
