package cz.uhk.ppro.model;

import cz.uhk.ppro.entities.Word;

public class PracticeWord {

    private Word wordForTranslation;

    private Word wordTranslated;

    private Integer correctCount;

    private Integer wrongCount;

    public PracticeWord(Word wordForTranslation, Word wordTranslated) {
        this.wordForTranslation = wordForTranslation;
        this.wordTranslated = wordTranslated;
        this.correctCount = 0;
        this.wrongCount = 0;
    }

    public Integer getScore() {
        return -wrongCount + correctCount;
    }

    public Integer getCorrectCount() {
        return correctCount;
    }

    public void setCorrectCount(Integer correctCount) {
        this.correctCount = correctCount;
    }

    public Integer getWrongCount() {
        return wrongCount;
    }

    public void setWrongCount(Integer wrongCount) {
        this.wrongCount = wrongCount;
    }

    public Word getWordForTranslation() {
        return wordForTranslation;
    }

    public void setWordForTranslation(Word wordForTranslation) {
        this.wordForTranslation = wordForTranslation;
    }

    public Word getWordTranslated() {
        return wordTranslated;
    }

    public void setWordTranslated(Word wordTranslated) {
        this.wordTranslated = wordTranslated;
    }

    public void incrementCorrectCount() {
        correctCount++;
    }

    public void incrementWrongCount() {
        wrongCount++;
    }

}
