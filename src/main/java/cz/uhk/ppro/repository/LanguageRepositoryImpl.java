package cz.uhk.ppro.repository;

import cz.uhk.ppro.entities.Language;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Transactional
@Repository
public class LanguageRepositoryImpl {
    private SessionFactory sessionFactory;

    public LanguageRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public List<Language> getItems() {
        Session session = CurrentSessionFactory.getSession(sessionFactory);
        List<Language> langList = session.createQuery("FROM Language").getResultList();
        session.close();
        return langList;
    }

}
