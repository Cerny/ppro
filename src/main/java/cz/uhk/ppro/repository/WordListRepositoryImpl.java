package cz.uhk.ppro.repository;

import java.util.List;
import cz.uhk.ppro.entities.Word;
import cz.uhk.ppro.entities.WordList;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository
public class WordListRepositoryImpl {

    private SessionFactory sessionFactory;

    public WordListRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public List<WordList> getItems() {
        Session session = CurrentSessionFactory.getSession(sessionFactory);
        List<WordList> wl = session.createQuery("FROM WordList", WordList.class).getResultList();
        session.close();
        return wl;
    }

    public WordList findById(int id) {
        Session session = CurrentSessionFactory.getSession(sessionFactory);
        WordList wl = (WordList) session.createQuery("FROM WordList list WHERE list.id =:id").setParameter("id", id).getSingleResult();
        session.close();
        return wl;
    }

    public void saveOrUpdateWL(WordList wl) {
        Session session = CurrentSessionFactory.getSession(sessionFactory);
        session.getTransaction().begin();
        session.saveOrUpdate(wl);
        session.getTransaction().commit();
        session.close();
    }

    public void saveNewWords(Word wd1, Word wd2, WordList wl){
        Session session = CurrentSessionFactory.getSession(sessionFactory);

        wd1.addTranslation(wd2);

        // Save words to DB
        session.getTransaction().begin();
        session.save(wd1);
        session.save(wd2);

        // Add word to the word list
        wl.addWord(wd1);
        wl.addWord(wd2);

        // Merge word list
        session.saveOrUpdate(wl);
        session.getTransaction().commit();
        session.close();
    }
    
}
