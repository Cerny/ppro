package cz.uhk.ppro.repository;

import cz.uhk.ppro.entities.PracticeDay;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository
public class PracticeDayRepositoryImpl {

    private SessionFactory sessionFactory;

    public PracticeDayRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void saveOrUpdatePracticeDay(PracticeDay practiceDay) {
        Session session = CurrentSessionFactory.getSession(sessionFactory);

        // Save the user in database
        session.beginTransaction();
        session.saveOrUpdate(practiceDay);
        session.getTransaction().commit();
    }

}