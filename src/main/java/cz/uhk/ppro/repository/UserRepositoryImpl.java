package cz.uhk.ppro.repository;

import cz.uhk.ppro.entities.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository
public class UserRepositoryImpl {

    private SessionFactory sessionFactory;

    public UserRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public User getUser(String email) {
        Session session = CurrentSessionFactory.getSession(sessionFactory);
        User user = session.find(User.class, email);
        session.close();
        return user;
    }

    public void saveOrUpdateUser(User usr) {
        Session session = CurrentSessionFactory.getSession(sessionFactory);
        session.getTransaction().begin();
        session.saveOrUpdate(usr);
        session.getTransaction().commit();
        session.close();
    }

}