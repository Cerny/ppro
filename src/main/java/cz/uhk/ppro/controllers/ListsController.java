package cz.uhk.ppro.controllers;

import cz.uhk.ppro.entities.User;
import cz.uhk.ppro.entities.WordList;
import cz.uhk.ppro.service.VocabularyServiceImpl;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedList;


@Controller
public class ListsController {

    private final VocabularyServiceImpl vocabularyService;

    public ListsController(VocabularyServiceImpl vocabularyService) {
        this.vocabularyService = vocabularyService;
    }

    @RequestMapping(value = "lists", method = RequestMethod.GET)
    public ModelAndView lists(Model model, HttpServletRequest request, HttpSession session){

        String usersEmail = (String) session.getAttribute("Email");
        User user = vocabularyService.findUserByEmail(usersEmail);
        LinkedList<WordList> usersWL = new LinkedList<WordList>();
        usersWL.addAll(user.getWordLists());
        WordList[] usersWLsArray = (WordList[]) usersWL.toArray(new WordList[usersWL.size()]);
        Arrays.sort(usersWLsArray, new SortByName());
        ModelAndView mapWL = new ModelAndView("lists");
        mapWL.addObject("users", usersWLsArray);

        return mapWL;
    }

    @RequestMapping(value = "lists/add/{wordListId}", method = RequestMethod.GET)
    public ModelAndView add(Model model, HttpServletRequest request, HttpSession session, @PathVariable("wordListId") int wordListId) {
        String usersEmail = (String) session.getAttribute("Email");
        User user = vocabularyService.findUserByEmail(usersEmail);
        WordList list = vocabularyService.findWordListById(wordListId);
        user.addList(list);
        vocabularyService.saveOrUpdateUser(user);
        return lists(model, request, session);
    }
}

class SortByName implements Comparator<WordList> {
    public int compare(WordList a, WordList b)  {
        return a.getName().compareTo(b.getName());
    }
}