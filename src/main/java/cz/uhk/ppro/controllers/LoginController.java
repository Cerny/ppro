package cz.uhk.ppro.controllers;

import cz.uhk.ppro.entities.User;
import cz.uhk.ppro.form.LoginForm;
import cz.uhk.ppro.form.RegisterForm;
import cz.uhk.ppro.service.VocabularyServiceImpl;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping("/login")
public class LoginController {

    private final VocabularyServiceImpl vocabularyService;

    public LoginController(VocabularyServiceImpl vocabularyService) {
        this.vocabularyService = vocabularyService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public String login(Model model, HttpSession session) {
        RegisterForm form = new RegisterForm();
        model.addAttribute("registerForm", form);
        return "login";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String loginProcess(@ModelAttribute("loginForm") LoginForm loginForm, BindingResult result, HttpServletRequest request, HttpSession session) {
        if (areCredentialsCorrect(loginForm.getEmail(), loginForm.getPassword())) {
            String mail = loginForm.getEmail();
            session.invalidate();
            HttpSession newSession = request.getSession();
            newSession.setAttribute("Email", mail);

            return "redirect:/";
        } else {
            result.reject("msg", "Wrong credentials.");
            return "login";
        }
    }

    private boolean areCredentialsCorrect(String email, String password) {
        User userFromDB = vocabularyService.findUserByEmail(email);
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        return userFromDB != null && encoder.matches(password, userFromDB.getPassword());
    }

}
