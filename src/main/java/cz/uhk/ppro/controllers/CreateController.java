package cz.uhk.ppro.controllers;

import cz.uhk.ppro.entities.Language;
import cz.uhk.ppro.entities.Word;
import cz.uhk.ppro.entities.WordList;
import cz.uhk.ppro.form.AddWordForm;
import cz.uhk.ppro.form.WordListForm;
import cz.uhk.ppro.model.PracticeWord;
import org.springframework.ui.Model;
import cz.uhk.ppro.service.VocabularyServiceImpl;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.LinkedList;
import java.util.List;

@Controller
public class CreateController {

    private final VocabularyServiceImpl vocabularyService;

    public CreateController(VocabularyServiceImpl vocabularyService) {
        this.vocabularyService = vocabularyService;
    }

    @InitBinder("wordListForm")
    public void initWordListBinder(WebDataBinder dataBinder) {
        dataBinder.setValidator(new CreateWordListFormValidator());
    }

    @InitBinder("addWordForm")
    public void initWordBinder(WebDataBinder dataBinder) {
        dataBinder.setValidator(new AddWordFormValidator());
    }

    @RequestMapping(value = "create", method = RequestMethod.GET)
    public String create(Model model) {

        List<Language> ll = vocabularyService.findLanguages();
        LinkedList<Language> langList = new LinkedList<Language>();
        langList.addAll(ll);
        model.addAttribute("languages", langList);
        return "create";
    }

    @RequestMapping(value = "create", method = RequestMethod.POST)
    public String createProcess(Model model, @Valid WordListForm wordListForm, BindingResult result) {
        if(result.hasErrors()){
            result.reject("msg", "Wrong input.");
            return create(model);
        } else {
            WordList wl = new WordList(wordListForm.getName(), wordListForm.getDescription(), wordListForm.getIso1(), wordListForm.getIso2());
            vocabularyService.saveOrUpdateWL(wl);
            return "redirect:/addWords/" + Integer.toString(wl.getId());
        }
    }

    private Word findWord(Word word, final String language) {
        if (word.getLanguage().equals(language)) return word;
        for (Word wordTranslation : word.getTranslations()) {
            if (wordTranslation.getLanguage().equals(language)) {
                return wordTranslation;
            }
        }
        return null;
    }

    @RequestMapping(value = "/addWords/{wordListId}", method = RequestMethod.GET)
    public String addWords(@PathVariable("wordListId") int wordListId, Model model) {
        WordList wl = vocabularyService.findWordListById(wordListId);
        model.addAttribute("list", wl);

        LinkedList<PracticeWord> wordsLinkedList = new LinkedList<PracticeWord>();
        for (Word word : wl.getWords()) {
            Word wordForTranslation = findWord(word, wl.getLang1());
            Word wordTranslated = findWord(word, wl.getLang2());
            if (wordForTranslation != null && wordTranslated != null) {
                wordsLinkedList.add(new PracticeWord(wordForTranslation, wordTranslated));
            }
        }
        model.addAttribute("words", wordsLinkedList);

        return "addWords";
    }

    @RequestMapping(value = "/addWords/{wordListId}", method = RequestMethod.POST)
    public String addWordsProcess(Model model, @Valid AddWordForm addWordForm, BindingResult result, @PathVariable("wordListId") int wordListId) {
        if (result.hasErrors()) {
            result.reject("msg", "Wrong input.");
        } else {
            WordList wl = vocabularyService.findWordListById(wordListId);
            Word wd1 = new Word(addWordForm.getWord1());
            Word wd2 = new Word(addWordForm.getWord2());
            wd1.setLanguage(wl.getLang1());
            wd2.setLanguage(wl.getLang2());

            vocabularyService.saveNewWords(wd1, wd2, wl);
        }

        return addWords(wordListId, model);
    }

}