package cz.uhk.ppro.controllers;

import cz.uhk.ppro.entities.PracticeDay;
import cz.uhk.ppro.entities.User;
import cz.uhk.ppro.model.PracticeWord;
import cz.uhk.ppro.service.VocabularyServiceImpl;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Set;

@Controller
public class StatisticsController {

    private VocabularyServiceImpl vocabularyService;

    public StatisticsController(VocabularyServiceImpl vocabularyService) {
        this.vocabularyService = vocabularyService;
    }

    @RequestMapping(value = "statistics")
    public String statistics(Model model, HttpSession session) {
        String userEmail = (String) session.getAttribute("Email");
        User loggedUser = vocabularyService.findUserByEmail(userEmail);

        Set<PracticeDay> practiceDays = loggedUser.getPracticeDays();
        PracticeDay practiceDaysArray[] = (PracticeDay[]) practiceDays.toArray(new PracticeDay[practiceDays.size()]);
        Arrays.sort(practiceDaysArray, new SortByDate());
        model.addAttribute("practiceDays", practiceDaysArray);

        return "statistics";
    }

}

class SortByDate implements Comparator<PracticeDay> {
    public int compare(PracticeDay a, PracticeDay b)  {
        return -(a.getPracticeDayPK().getDate().compareTo(b.getPracticeDayPK().getDate()));
    }
}