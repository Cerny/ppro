package cz.uhk.ppro.controllers;

import cz.uhk.ppro.form.AddWordForm;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

public class AddWordFormValidator implements Validator {

    private static final String REQUIRED = "required";

    public void validate(Object o, Errors errors) {
        AddWordForm addWordForm = (AddWordForm) o;

        if (!(addWordForm.getWord1().length() > 0)) {
            errors.rejectValue("word1", REQUIRED);
        }

        if (!(addWordForm.getWord2().length() > 0)) {
            errors.rejectValue("word2", REQUIRED);
        }
    }

    public boolean supports(Class<?> aClass) {
        return AddWordForm.class.isAssignableFrom(aClass);
    }

}