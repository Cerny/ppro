package cz.uhk.ppro.controllers;

import cz.uhk.ppro.form.RegisterForm;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;


public class RegisterFormValidator implements Validator {

    private static final String REQUIRED = "required";

    public void validate(Object o, Errors errors) {
        RegisterForm registerForm = (RegisterForm) o;

        if (!(registerForm.getEmail().length() > 0)) {
            errors.rejectValue("email", REQUIRED);
        }

        if (!(registerForm.getName().length()>0)) {
            errors.rejectValue("name", REQUIRED);
        }

        if (!(registerForm.getSurname().length()>0)) {
            errors.rejectValue("surname", REQUIRED);
        }

        String password = registerForm.getPassword();
        String confirmPassword = registerForm.getConfirmPassword();
        if (!(password.length() > 0)) {
            errors.rejectValue("password", REQUIRED);
        }

        if (!(password.equals(confirmPassword))) {
            errors.rejectValue("password", "DIFFERS");
        }
    }

    public boolean supports(Class<?> aClass) {
        return RegisterForm.class.isAssignableFrom(aClass);
    }

}
