package cz.uhk.ppro.controllers;

import cz.uhk.ppro.entities.*;
import cz.uhk.ppro.form.WordForm;
import cz.uhk.ppro.model.PracticeWord;
import cz.uhk.ppro.service.VocabularyServiceImpl;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
public class PracticeController {

    private final VocabularyServiceImpl vocabularyService;

    public PracticeController(VocabularyServiceImpl vocabularyService) {
        this.vocabularyService = vocabularyService;
    }

    @RequestMapping("practice/{wordListId}")
    public String startPractice(Model model,
                                @PathVariable("wordListId") int wordListId,
                                @RequestParam("languageFrom") final String languageFrom,
                                @RequestParam("languageTo") String languageTo,
                                HttpServletRequest request,
                                HttpSession session) {
        // Obtain words and transform them to the array
        WordList list = vocabularyService.findWordListById(wordListId);
        LinkedList<PracticeWord> wordsLinkedList = new LinkedList<PracticeWord>();
        for (Word word : list.getWords()) {
            Word wordForTranslation = findWord(word, languageFrom);
            Word wordTranslated = findWord(word, languageTo);
            if (wordForTranslation != null && wordTranslated != null) {
                wordsLinkedList.add(new PracticeWord(wordForTranslation, wordTranslated));
            }
        }

        // Save words to the current session
        PracticeWord[] wordsArray = (PracticeWord[]) wordsLinkedList.toArray(new PracticeWord[wordsLinkedList.size()]);
        request.getSession().setAttribute("wordsList", wordsArray);

        // Setup word index to 0
        session.setAttribute("wordIndex", 0);

        if (list.getWords().size() == 0) {
            request.setAttribute("emptyList", true);
            model.addAttribute(list);
            return "practiceOverview";
        } else {
            return nextWord(model, session);
        }
    }

    private Word findWord(Word word, final String language) {
        if (word.getLanguage().equals(language)) return word;
        for (Word wordTranslation : word.getTranslations()) {
            if (wordTranslation.getLanguage().equals(language)) {
                return wordTranslation;
            }
        }
        return null;
    }

    @RequestMapping(value = "practice/check")
    public String checkAnswer(@ModelAttribute("wordForm") WordForm wordForm,
                              Model model,
                              HttpServletRequest request,
                              HttpSession session) {
        // Get words and word index from current session
        PracticeWord[] words = getWordsFrom(session);
        Integer wordIndex = (Integer) session.getAttribute("wordIndex");

        // Check the answer
        String wordFromInput = wordForm.getWord();
        PracticeWord currentWord = words[0];
        Boolean isAnswerCorrect = wordFromInput.equals(currentWord.getWordTranslated().getSpelling());
        request.setAttribute("correctAnswer", isAnswerCorrect);
        model.addAttribute("previousAnswer", wordFromInput);

        // Setup score
        if (isAnswerCorrect) {
            currentWord.incrementCorrectCount();
        } else {
            currentWord.incrementWrongCount();
        }

        request.getSession().setAttribute("wordsList", words);

        // Setup current word to the model
        model.addAttribute("wordForTranslation", words[0].getWordForTranslation());
        model.addAttribute("wordTranslated", words[0].getWordTranslated());

        return "practiceShowAnswer";
    }

    @RequestMapping(value = "practice/skip")
    public String nextWord(@ModelAttribute("wordForm") WordForm wordForm) {
        return "redirect:/practice/next";
    }

    @RequestMapping(value = "practice/next")
    public String nextWord(Model model,
                           HttpSession session) {
        // Get words and word index from current session
        PracticeWord[] wordsArray = getWordsFrom(session);
        Integer wordIndex = (Integer) session.getAttribute("wordIndex");

        // Prepare array of words
        Arrays.sort(wordsArray, new SortByScore());
        // Shuffle up to first 3 items of the array
        Random randomGenerator = new Random();  // Random number generator
        for (int i = 0; i < 3 && i < wordsArray.length - 1; i++) {
            int randomPosition = randomGenerator.nextInt(wordsArray.length);
            PracticeWord temp = wordsArray[i];
            wordsArray[i] = wordsArray[randomPosition];
            wordsArray[randomPosition] = temp;
        }

        // Prepare next word and set it to the model
        Word wordToTranslate = wordsArray[0].getWordForTranslation();
        model.addAttribute("wordForTranslation", wordToTranslate);
        session.setAttribute("wordIndex", wordIndex);

        return "practiceTypeAnswer";
    }

    private PracticeWord[] getWordsFrom(HttpSession session) {
        return (PracticeWord[]) session.getAttribute("wordsList");
    }

    @RequestMapping(value = "practice/finish")
    public String practiceDayFinish(Model model, HttpSession session) {
        PracticeWord[] wordsArray = getWordsFrom(session);
        Arrays.sort(wordsArray, new SortByScore());

        // Save data to statistics
        PracticeDay practiceDay = getPracticeDay(wordsArray, (String) session.getAttribute("Email"));
        vocabularyService.saveOrUpdatePracticeDay(practiceDay);

        model.addAttribute("words", wordsArray);
        return "statisticsAfterPractice";
    }

    private PracticeDay getPracticeDay(PracticeWord[] words, String userEmail) {
        // Count correct and wrong answers for statistics
        Integer correctCount = 0;
        Integer wrongCount = 0;
        for (PracticeWord word: words) {
            correctCount += word.getCorrectCount();
            wrongCount += word.getWrongCount();
        }

        User loggedUser = vocabularyService.findUserByEmail(userEmail);

        // Find practice day in DB
        PracticeDay practiceDayDB = null;
        Date now = new Date();
        for (PracticeDay practiceDay: loggedUser.getPracticeDays()) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
            if (sdf.format(practiceDay.getPracticeDayPK().getDate()).equals(sdf.format(now))) {
                practiceDayDB = practiceDay;
                break;
            }
        }

        // Create or update practice day
        if (practiceDayDB == null) {
            PracticeDayPK practiceDayPK = new PracticeDayPK(new Date(), loggedUser);
            return new PracticeDay(practiceDayPK, correctCount, wrongCount);
        } else {
            practiceDayDB.setCorrectCount(practiceDayDB.getCorrectCount() + correctCount);
            practiceDayDB.setWrongCount(practiceDayDB.getWrongCount() + wrongCount);
            return practiceDayDB;
        }
    }

}

class SortByScore implements Comparator<PracticeWord> {
    public int compare(PracticeWord a, PracticeWord b)  {
        return a.getScore() - b.getScore();
    }
}
