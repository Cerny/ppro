package cz.uhk.ppro.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/")
public class HomeController {

    @RequestMapping(method = RequestMethod.GET)
    public String home(Model model, HttpSession session) {
        if (session.getAttribute("Email") != null) {
            return "redirect:/lists";
        } else {
            return "redirect:/login";
        }
    }

}
