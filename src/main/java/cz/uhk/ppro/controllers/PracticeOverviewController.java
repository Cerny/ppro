package cz.uhk.ppro.controllers;

import cz.uhk.ppro.entities.WordList;
import cz.uhk.ppro.service.VocabularyServiceImpl;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class PracticeOverviewController {

    private final VocabularyServiceImpl vocabularyService;

    public PracticeOverviewController(VocabularyServiceImpl vocabularyService) {
        this.vocabularyService = vocabularyService;
    }

    @RequestMapping("/practiceOverview/{wordListId}")
    public ModelAndView showOverview(@PathVariable("wordListId") int wordListId) {
        ModelAndView mav = new ModelAndView("practiceOverview");
        WordList wordList = vocabularyService.findWordListById(wordListId);
        mav.addObject(wordList);
        return mav;
    }

}
