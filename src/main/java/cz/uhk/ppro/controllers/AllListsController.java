package cz.uhk.ppro.controllers;

import cz.uhk.ppro.entities.WordList;
import cz.uhk.ppro.service.VocabularyServiceImpl;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.LinkedList;
import java.util.List;

@Controller
@RequestMapping("/allLists")
public class AllListsController {

    private final VocabularyServiceImpl vocabularyService;

    public AllListsController(VocabularyServiceImpl vocabularyService) {
        this.vocabularyService = vocabularyService;
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public ModelAndView allLists(Model model, HttpServletRequest request, HttpSession session){

        List<WordList> wlList = vocabularyService.findWordLists();
        LinkedList<WordList> linkedWL = new LinkedList<WordList>();
        linkedWL.addAll(wlList);
        ModelAndView mapWL = new ModelAndView("allLists");
        mapWL.addObject("lists", linkedWL);

        return mapWL;
    }

}
