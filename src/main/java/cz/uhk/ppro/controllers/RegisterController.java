package cz.uhk.ppro.controllers;

import cz.uhk.ppro.entities.User;
import cz.uhk.ppro.form.RegisterForm;
import cz.uhk.ppro.service.VocabularyServiceImpl;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
@RequestMapping("/")
public class RegisterController {

    private final VocabularyServiceImpl vocabularyService;

    public RegisterController(VocabularyServiceImpl vocabularyService) {
        this.vocabularyService = vocabularyService;
    }

    @InitBinder("registerForm")
    public void initRegisterFormBinder(WebDataBinder dataBinder) {
        dataBinder.setValidator(new RegisterFormValidator());
    }

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public String register() {
        return "register";
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String registerProcess(@Valid RegisterForm registerForm, BindingResult result, HttpServletRequest request, HttpSession session) {
        if (vocabularyService.findUserByEmail(registerForm.getEmail()) != null) {
            result.reject("msg", "User already exists.");
            return "register";
        }

        if (!result.hasErrors()) {
            String mail = registerForm.getEmail();
            session.invalidate();
            HttpSession newSession = request.getSession();
            newSession.setAttribute("Email", mail);

            // Create user in DB
            BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
            String encodedPassword = encoder.encode(registerForm.getPassword());
            User user = new User(registerForm.getEmail(), encodedPassword, registerForm.getName(), registerForm.getSurname());
            vocabularyService.saveOrUpdateUser(user);

            return "redirect:/";
        } else {
            result.reject("msg", "Wrong input.");
            return "register";
        }
    }

}
