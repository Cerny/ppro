package cz.uhk.ppro.controllers;

import cz.uhk.ppro.form.WordListForm;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

public class CreateWordListFormValidator implements Validator {

    private static final String REQUIRED = "required";

    public void validate(Object o, Errors errors) {
        WordListForm wordListForm = (WordListForm) o;

        if (!(wordListForm.getName().length() > 0)) {
            errors.rejectValue("name", REQUIRED);
        }

        if (!(wordListForm.getIso1().length() > 0)) {
            errors.rejectValue("iso1", REQUIRED);
        }

        if (!(wordListForm.getIso2().length() > 0)) {
            errors.rejectValue("iso2", REQUIRED);
        }
    }

    public boolean supports(Class<?> aClass) {
        return WordListForm.class.isAssignableFrom(aClass);
    }

}
