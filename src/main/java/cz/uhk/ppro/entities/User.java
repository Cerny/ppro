package cz.uhk.ppro.entities;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.Set;

@Entity
@Table(name = "T_USER")
public class User {

    @Id
    @NotEmpty
    @Column(name = "U_EMAIL")
    private String email;

    @Column(name = "U_PASSWORD")
    @NotEmpty
    private String password;

    @Column(name = "U_NAME")
    @NotEmpty
    private String name;

    @Column(name = "U_SURNAME")
    @NotEmpty
    private String surname;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name="T_SELECTEDLIST",
        joinColumns = @JoinColumn(name="T_USER_EMAIL"),
        inverseJoinColumns = @JoinColumn(name="T_WORDLIST_ID"))
    private Set<WordList> wordLists;

    @OneToMany(fetch = FetchType.EAGER, mappedBy="practiceDayPK.user")
    private Set<PracticeDay> practiceDays;

    public User() {
    }

    public User(String email, String password, String name, String surname) {
        this.email = email;
        this.password = password;
        this.name = name;
        this.surname = surname;
    }

    public Set<PracticeDay> getPracticeDays() {
        return practiceDays;
    }

    public void setPracticeDays(Set<PracticeDay> practiceDays) {
        this.practiceDays = practiceDays;
    }

    public Set<WordList> getWordLists() {
        return wordLists;
    }

    public void setWordLists(Set<WordList> wordLists) {
        this.wordLists = wordLists;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void addList(WordList wl) {
        wordLists.add(wl);
        wl.getUsers().add(this);
    }

}