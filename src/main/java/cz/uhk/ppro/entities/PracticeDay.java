package cz.uhk.ppro.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "T_PRACTICEDAY")
public class PracticeDay implements Serializable {

    @EmbeddedId
    private PracticeDayPK practiceDayPK;

    @Column(name = "D_WRONGCOUNT")
    private Integer wrongCount;

    @Column(name = "D_CORRECTCOUNT")
    private Integer correctCount;

    public PracticeDayPK getPracticeDayPK() {
        return practiceDayPK;
    }

    public PracticeDay() {
    }

    public PracticeDay(PracticeDayPK practiceDayPK, Integer correctCount, Integer wrongCount) {
        this.practiceDayPK = practiceDayPK;
        this.wrongCount = wrongCount;
        this.correctCount = correctCount;
    }

    public void setPracticeDayPK(PracticeDayPK practiceDayPK) {
        this.practiceDayPK = practiceDayPK;
    }

    public Integer getWrongCount() {
        return wrongCount;
    }

    public void setWrongCount(Integer wrongCount) {
        this.wrongCount = wrongCount;
    }

    public Integer getCorrectCount() {
        return correctCount;
    }

    public void setCorrectCount(Integer correctCount) {
        this.correctCount = correctCount;
    }

}
