package cz.uhk.ppro.entities;


import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;
import java.util.Date;

@Embeddable
public class PracticeDayPK implements Serializable {

    @Column(name = "D_DATE")
    protected Date date;

    @ManyToOne
    @JoinColumn(name="T_USER_EMAIL")
    protected User user;

    public PracticeDayPK() {}

    public PracticeDayPK(Date date, User user) {
        this.date = date;
        this.user = user;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

}
