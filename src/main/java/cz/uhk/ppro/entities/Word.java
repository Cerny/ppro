package cz.uhk.ppro.entities;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "T_WORD")
public class Word {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "wid_gen")
    @SequenceGenerator(name = "wid_gen", sequenceName = "word_seq", allocationSize = 1)
    @Column(name = "W_ID")
    private Integer id;

    @Column(name = "W_SPELLING")
    private String spelling;

    @Column(name = "T_LANGUAGE_ISO")
    private String language;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "T_TRANSLATION",
            joinColumns = @JoinColumn(name = "T_WORD_ID"),
            inverseJoinColumns = @JoinColumn(name = "T_WORD_ID1"))
    private Set<Word> translations;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "T_TRANSLATION",
            joinColumns = @JoinColumn(name = "T_WORD_ID1"),
            inverseJoinColumns = @JoinColumn(name = "T_WORD_ID"))
    private Set<Word> translationsOf;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSpelling() {
        return spelling;
    }

    public void setSpelling(String spelling) {
        this.spelling = spelling;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public Set<Word> getTranslations() {
        return translations;
    }

    public void setTranslations(Set<Word> translations) {
        this.translations = translations;
    }

    public Set<Word> getTranslationsOf() {
        return translationsOf;
    }

    public void setTranslationsOf(Set<Word> translationsOf) {
        this.translationsOf = translationsOf;
    }

    public void addTranslation(Word wd)
    {
        translations.add(wd);
    }

    public Word() {
    }

    public Word(Integer id, String spelling) {
        this.id=id;
        this.spelling=spelling;
    }

    public Word(String spelling) {
        this.spelling=spelling;
        this.translations = new HashSet<Word>();
        this.translationsOf = new HashSet<Word>();
    }

}