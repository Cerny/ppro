package cz.uhk.ppro.entities;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "T_WORDLIST")
public class WordList {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "wlid_gen")
    @SequenceGenerator(name = "wlid_gen", sequenceName = "wordlist_seq", allocationSize = 1)
    @Column(name = "WL_ID")
    private Integer id;

    @Column(name = "WL_NAME")
    private String name;

    @Column(name = "WL_DESCRIPTION")
    private String description;

    @Column(name = "T_LANGUAGE_ISO")
    private String lang1;

    @Column(name = "T_LANGUAGE_ISO2")
    private String lang2;

    @ManyToMany(fetch = FetchType.EAGER, mappedBy = "wordLists")
    private Set<User> users;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "T_INCLUDEDWORD",
            joinColumns = @JoinColumn(name = "T_WORDLIST_ID"),
            inverseJoinColumns = @JoinColumn(name = "T_WORD_ID"))
    private Set<Word> words;

    public WordList() {
    }


    public WordList(Integer id, String name, String description, String lang1, String lang2) {
        this.id=id;
        this.name=name;
        this.description=description;
        this.lang1=lang1;
        this.lang2=lang2;
    }

    public WordList(String name, String description, String lang1, String lang2) {
        this.name=name;
        this.description=description;
        this.lang1=lang1;
        this.lang2=lang2;
    }

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getLang1() {
        return lang1;
    }

    public String getLang2() {
        return lang2;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setLang1(String lang1) {
        this.lang1 = lang1;
    }

    public void setLang2(String lang2) {
        this.lang2 = lang2;
    }

    public Set<Word> getWords() {
        return words;
    }

    public void setWords(Set<Word> words) {
        this.words = words;
    }

    public void addWord(Word wd) {
        words.add(wd);
    }

}
