package cz.uhk.ppro.service;

import cz.uhk.ppro.entities.*;
import cz.uhk.ppro.repository.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class VocabularyServiceImpl {

    private UserRepositoryImpl userRepository;
    private WordListRepositoryImpl wordListRepository;
    private LanguageRepositoryImpl languageRepository;
    private PracticeDayRepositoryImpl practiceDayRepository;

    public VocabularyServiceImpl(UserRepositoryImpl userRepository, WordListRepositoryImpl wordListRepository,
                                 PracticeDayRepositoryImpl practiceDayRepository, LanguageRepositoryImpl languageRepository) {
        this.languageRepository = languageRepository;
        this.userRepository = userRepository;
        this.wordListRepository = wordListRepository;
        this.practiceDayRepository = practiceDayRepository;
    }

    @Transactional(readOnly = true)
    public User findUserByEmail(String email) {
        return userRepository.getUser(email);
    }

    @Transactional
    public void saveOrUpdateUser(User user) {
        userRepository.saveOrUpdateUser(user);
    }

    @Transactional
    public void saveOrUpdatePracticeDay(PracticeDay practiceDay) {
        practiceDayRepository.saveOrUpdatePracticeDay(practiceDay);
    }

    @Transactional(readOnly = true)
    public List<WordList> findWordLists() {
        return wordListRepository.getItems();
    }

    @Transactional(readOnly = true)
    public WordList findWordListById(int id) {
        return wordListRepository.findById(id);
    }

    @Transactional
    public void saveOrUpdateWL(WordList wl) {
        wordListRepository.saveOrUpdateWL(wl);
    }

    @Transactional(readOnly = true)
    public List<Language> findLanguages() {
        return languageRepository.getItems();
    }

    @Transactional
    public void saveNewWords(Word wd1, Word wd2, WordList wl) {
        wordListRepository.saveNewWords(wd1, wd2, wl);
    }

}
