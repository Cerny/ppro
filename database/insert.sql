-----------------------------------------------------------------------------------------------------------------
--initial testing data
-----------------------------------------------------------------------------------------------------------------

Insert into T_USER (U_EMAIL,U_PASSWORD,U_NAME,U_SURNAME) values ('petr.urban@gmail.com','$2a$10$mGaL/uTKvmsZG/fX7RljgOHmR95wRR9QEKMGP1ERkUw4tF/lvhTSu','Petr','Urban'); -- password: "heslo11"

Insert into T_LANGUAGE (L_ISO,L_NAME) values ('cs','cestina');
Insert into T_LANGUAGE (L_ISO,L_NAME) values ('en','anglictina');

Insert into T_WORD (W_ID,W_SPELLING,T_LANGUAGE_ISO) values ('00001','ahoj','cs');
Insert into T_WORD (W_ID,W_SPELLING,T_LANGUAGE_ISO) values ('00002','hello','en');

Insert into T_WORD (W_ID,W_SPELLING,T_LANGUAGE_ISO) values ('00003','hezky','cs');
Insert into T_WORD (W_ID,W_SPELLING,T_LANGUAGE_ISO) values ('00004','nice','en');

Insert into T_WORD (W_ID,W_SPELLING,T_LANGUAGE_ISO) values ('00005','dum','cs');
Insert into T_WORD (W_ID,W_SPELLING,T_LANGUAGE_ISO) values ('00006','house','en');

Insert into T_WORD (W_ID,W_SPELLING,T_LANGUAGE_ISO) values ('00007','zima','cs');
Insert into T_WORD (W_ID,W_SPELLING,T_LANGUAGE_ISO) values ('00008','winter','en');

Insert into T_WORD (W_ID,W_SPELLING,T_LANGUAGE_ISO) values ('00009','leto','cs');
Insert into T_WORD (W_ID,W_SPELLING,T_LANGUAGE_ISO) values ('00010','summer','en');

Insert into T_WORD (W_ID,W_SPELLING,T_LANGUAGE_ISO) values ('00011','podzim','cs');
Insert into T_WORD (W_ID,W_SPELLING,T_LANGUAGE_ISO) values ('00012','autumn','en');

Insert into T_TRANSLATION (T_WORD_ID,T_WORD_ID1) values ('00001','00002');
Insert into T_TRANSLATION (T_WORD_ID,T_WORD_ID1) values ('00003','00004');
Insert into T_TRANSLATION (T_WORD_ID,T_WORD_ID1) values ('00005','00006');
Insert into T_TRANSLATION (T_WORD_ID,T_WORD_ID1) values ('00007','00008');
Insert into T_TRANSLATION (T_WORD_ID,T_WORD_ID1) values ('00009','00010');
Insert into T_TRANSLATION (T_WORD_ID,T_WORD_ID1) values ('00011','00012');

Insert into T_WORDLIST (WL_ID,WL_NAME,WL_DESCRIPTION,T_LANGUAGE_ISO,T_LANGUAGE_ISO2) values ('001','zaklad cs-en','zakladni seznam slovicek mezi anglickym a ceskym jazykem','cs','en');
Insert into T_WORDLIST (WL_ID,WL_NAME,WL_DESCRIPTION,T_LANGUAGE_ISO,T_LANGUAGE_ISO2) values ('002','pokrocili cs-en','pokrocily seznam slovicek mezi anglickym a ceskym jazykem','cs','en');

Insert into T_INCLUDEDWORD (T_WORD_ID,T_WORDLIST_ID) values ('00001','001');
Insert into T_INCLUDEDWORD (T_WORD_ID,T_WORDLIST_ID) values ('00002','001');
Insert into T_INCLUDEDWORD (T_WORD_ID,T_WORDLIST_ID) values ('00003','001');
Insert into T_INCLUDEDWORD (T_WORD_ID,T_WORDLIST_ID) values ('00004','001');
Insert into T_INCLUDEDWORD (T_WORD_ID,T_WORDLIST_ID) values ('00005','002');
Insert into T_INCLUDEDWORD (T_WORD_ID,T_WORDLIST_ID) values ('00006','002');
Insert into T_INCLUDEDWORD (T_WORD_ID,T_WORDLIST_ID) values ('00007','002');
Insert into T_INCLUDEDWORD (T_WORD_ID,T_WORDLIST_ID) values ('00008','002');
Insert into T_INCLUDEDWORD (T_WORD_ID,T_WORDLIST_ID) values ('00009','002');
Insert into T_INCLUDEDWORD (T_WORD_ID,T_WORDLIST_ID) values ('00010','002');

Insert into T_PRACTICEDAY (D_DATE,D_CORRECTCOUNT,D_WRONGCOUNT,T_USER_EMAIL) values (TO_DATE('2003/05/03', 'yyyy/mm/dd'),'0','2','petr.urban@gmail.com');
Insert into T_PRACTICEDAY (D_DATE,D_CORRECTCOUNT,D_WRONGCOUNT,T_USER_EMAIL) values (TO_DATE('2003/05/27', 'yyyy/mm/dd'),'2','0','petr.urban@gmail.com');
Insert into T_PRACTICEDAY (D_DATE,D_CORRECTCOUNT,D_WRONGCOUNT,T_USER_EMAIL) values (to_date('07.01.20','DD.MM.RR'),'7','3','petr.urban@gmail.com');
