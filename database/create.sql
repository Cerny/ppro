CREATE SEQUENCE wordlist_seq
START WITH 300
INCREMENT BY 1
MINVALUE 300
MAXVALUE 999999999
NOCYCLE;

CREATE SEQUENCE word_seq
START WITH 15000
INCREMENT BY 1
MINVALUE 15000
MAXVALUE 999999999
NOCYCLE;


CREATE TABLE t_includedword (
    t_word_id       INTEGER NOT NULL,
    t_wordlist_id   INTEGER NOT NULL
);

ALTER TABLE t_includedword ADD CONSTRAINT t_includedword_pk PRIMARY KEY ( t_word_id,
                                                                      t_wordlist_id );

CREATE TABLE t_language (
    l_iso    VARCHAR2(5 CHAR) NOT NULL,
    l_name   VARCHAR2(30 CHAR) NOT NULL
);

ALTER TABLE t_language ADD CONSTRAINT t_language_pk PRIMARY KEY ( l_iso );

CREATE TABLE t_practiceday (
    d_date         DATE NOT NULL,
    d_correctcount   INTEGER NOT NULL,
    d_wrongcount     INTEGER NOT NULL,
    t_user_email     VARCHAR2(40 CHAR) NOT NULL
);

ALTER TABLE t_practiceday ADD CONSTRAINT t_practiceday_pk PRIMARY KEY ( d_date,
                                                                    t_user_email );

CREATE TABLE t_selectedlist (
    t_user_email    VARCHAR2(60 CHAR) NOT NULL,
    t_wordlist_id   INTEGER NOT NULL
);

ALTER TABLE t_selectedlist ADD CONSTRAINT t_selectedlist_pk PRIMARY KEY ( t_user_email,
                                                                      t_wordlist_id );

CREATE TABLE t_translation (
    t_word_id    INTEGER NOT NULL,
    t_word_id1   INTEGER NOT NULL
);

ALTER TABLE t_translation ADD CONSTRAINT t_translation_pk PRIMARY KEY ( t_word_id,
                                                                    t_word_id1 );

CREATE TABLE t_user (
    u_email      VARCHAR2(60 CHAR) NOT NULL,
    u_password   VARCHAR2(70 CHAR) NOT NULL,
    u_name       VARCHAR2(20 CHAR),
    u_surname    VARCHAR2(20 CHAR)
);

ALTER TABLE t_user ADD CONSTRAINT t_user_pk PRIMARY KEY ( u_email );

CREATE TABLE t_word (
    w_id             INTEGER NOT NULL,
    w_spelling       VARCHAR2(30 CHAR) NOT NULL,
    t_language_iso   VARCHAR2(5 CHAR) NOT NULL
);

ALTER TABLE t_word ADD CONSTRAINT t_word_pk PRIMARY KEY ( w_id );

CREATE TABLE t_wordlist (
    wl_id              INTEGER NOT NULL,
    wl_name            VARCHAR2(40 CHAR) NOT NULL,
    wl_description     VARCHAR2(100 CHAR),
    t_language_iso    VARCHAR2(5 CHAR) NOT NULL,
    t_language_iso2   VARCHAR2(5 CHAR) NOT NULL
);

ALTER TABLE t_wordlist ADD CONSTRAINT t_wordlist_pk PRIMARY KEY ( wl_id );

ALTER TABLE t_includedword
    ADD CONSTRAINT t_includedword_word_fk FOREIGN KEY ( t_word_id )
        REFERENCES t_word ( w_id );

ALTER TABLE t_includedword
    ADD CONSTRAINT t_includedword_wordlist_fk FOREIGN KEY ( t_wordlist_id )
        REFERENCES t_wordlist ( wl_id );

ALTER TABLE t_practiceday
    ADD CONSTRAINT t_practiceday_user_fk FOREIGN KEY ( t_user_email )
        REFERENCES t_user ( u_email );

ALTER TABLE t_selectedlist
    ADD CONSTRAINT t_selectedlist_user_fk FOREIGN KEY ( t_user_email )
        REFERENCES t_user ( u_email );

ALTER TABLE t_selectedlist
    ADD CONSTRAINT t_selectedlist_wordlist_fk FOREIGN KEY ( t_wordlist_id )
        REFERENCES t_wordlist ( wl_id );

ALTER TABLE t_translation
    ADD CONSTRAINT t_translation_word_fk FOREIGN KEY ( t_word_id )
        REFERENCES t_word ( w_id );

ALTER TABLE t_translation
    ADD CONSTRAINT t_translation_word_fkv1 FOREIGN KEY ( t_word_id1 )
        REFERENCES t_word ( w_id );

ALTER TABLE t_word
    ADD CONSTRAINT t_word_language_fk FOREIGN KEY ( t_language_iso )
        REFERENCES t_language ( l_iso );

ALTER TABLE t_wordlist
    ADD CONSTRAINT t_wordlist_language_fk FOREIGN KEY ( t_language_iso )
        REFERENCES t_language ( l_iso );

ALTER TABLE t_wordlist
    ADD CONSTRAINT t_wordlist_language_fkv1 FOREIGN KEY ( t_language_iso2 )
        REFERENCES t_language ( l_iso );

